/* Chekear que la pagina cargo */
window.onload = Main;

/* array "asociativo" */
/*var array = {jabon:15, leche:17};*/

/* revisar tema dado de Objetos */
var contador = 0;
var boton;

var body;
var header;
var headSpan;
var section;
var matrizDiv;
var matrizSuDoKu;
var arrayCtr;

var xSudo;
var ySudo;
var jSudo;
var kSudo;
var input;
var verif;
var mostrar;

/* Funciones */
function Main(){




	matrizSuDoKu = GenerarSuDoKu(GenerarMatrizAleatoria());


	section = document.getElementById("section");
	matrizDiv = PoblarHtmDyn();

	matrizSuDoKu=GenerarHuecos(matrizSuDoKu)
	MostrarSuDoku(matrizSuDoKu,matrizDiv);






/*	RecorrerFila(0,matrizSuDoKu);
  // retocar titulos de botones
/*	arrayDiv[0].innerHTML = "Fondo Verde";
	arrayDiv[1].innerHTML = "Fondo Blanco";*/

}
function GenerarHuecos(matrizSuDoKu){
	var cantHuecos = 0;

	cantHuecos = parseInt(localStorage.getItem("huecos"));
//	alert("cantHuecos" + cantHuecos);


	var rnd = Math.floor((Math.random() * 80) + 1);
	var arrayHuecos = [];
	var xSudo;
	var ySudo;
	var jSudo;
	var kSudo;
	var xx;
	var yy;


	while (arrayHuecos.length < cantHuecos){
		while (arrayHuecos.indexOf(rnd)>=0){
		  rnd = Math.floor((Math.random() * 80) + 1);
		}
		arrayHuecos.push(rnd);
	}
//	alert("Huecos:"+arrayHuecos);
	for (var ii=0;ii<cantHuecos;ii++){
	  xx=(parseInt(arrayHuecos[ii] / 9 ));
	  yy=arrayHuecos[ii] % 9;
	  xSudo = parseInt(xx / 3);
	  ySudo = parseInt(yy / 3);
	  jSudo = xx % 3;
	  kSudo = yy % 3;
//	  alert("HUecos:" +arrayHuecos[ii] + " xx:" + xx + " yy:" + yy + " xSudo:" + xSudo + " ySudo:" + ySudo + " jSudo:" + jSudo + " kSudo:" + kSudo +  " valor:" + matrizSuDoKu[xSudo][ySudo][jSudo][kSudo]);
	  matrizSuDoKu[xSudo][ySudo][jSudo][kSudo]=0; // falta desasignar el evento y mostrarlos read only

    }
	return matrizSuDoKu;
}
function PoblarHtmDyn(){
	var matrizDiv = [];

	//  crear los div para mostrar los numeros del sudoku. meter 9 filas en la matriz.

	for (var ii = 0 ; ii < 9;ii++){
		matrizDiv.push( Fila(ii) );
	}



	// Generar el boton para invocar a la verificaicon del sudoku
	// eventualmente se puede lanzar la rutina con cada edicion, pero de momento conviene tenerlo asi.
/*	verif = document.createElement("input");
	verif.type = "button";
	verif.style.width = "80px";
	verif.style.height = "20px";
	verif.value = "Verificar";
	verif.style.marginLeft = "200px";
	verif.onclick = BtnVerifClick;
	section.appendChild(verif);

	mostrar = document.createElement("input");
    mostrar.type = "button";
	mostrar.style.width = "80px";
	mostrar.style.height = "20px";
	mostrar.value = "Mostrar";
	mostrar.style.marginLeft = "210px";
	mostrar.onclick = BtnMostrarClick;
	section.appendChild(mostrar);*/
	arrayCtr = [];
	for (var ii=0;ii<10;ii++){

		arrayCtr.push(document.createElement("div"));
		arrayCtr[ii].id 				= "ctr" + ii;

		// estilos del div
		arrayCtr[ii].style.width 	= "28px";
	    arrayCtr[ii].style.height 	= "28px";


	    arrayCtr[ii].style.display 		 = "inline-block";
		arrayCtr[ii].style.textAlign     = "center";
		arrayCtr[ii].style.lineHeight    = arrayCtr[ii].style.height;
	    arrayCtr[ii].style.verticalAlign = "bottom";

	    arrayCtr[ii].style.marginTop	 = "10px";
	    arrayCtr[ii].innerHTML 			 = ii;
	    arrayCtr[ii].onclick     		 = FCtrClick;
	    if (ii == 0){
			arrayCtr[ii].innerHTML = "";
			arrayCtr[ii].style.borderLeft 	     = "2px solid Orange";
			arrayCtr[ii].style.marginLeft              = "11px";
		}else {
			arrayCtr[ii].style.borderLeft 	     = "1px solid Orange";
		}

		if (ii == 9){
			arrayCtr[ii].style.borderRight 	     = "2px solid Orange";
		}

		arrayCtr[ii].style.borderTop = "2px solid Orange";
		arrayCtr[ii].style.borderBottom = "2px solid Orange";


	    section.appendChild(arrayCtr[ii]);

	}

	// generar el campo de input que permite editar el sudoku
/*		input = document.createElement("input");
		input.type = "number";
		input.max = "9";
		input.min = "0";

		input.style.width = "25px";
		input.style.height = "15px";
		input.style.marginLeft = "10px";
		section.appendChild(input);*/



	return matrizDiv;
}

function Fila(fila){
/* retornar un vector de 9 posiciones
donde cada posicion sea un cuadrito de div a mostrar en el HTML*/

	var arrayDiv = [];
	for (var jj = 0 ; jj < 9; jj++){
	  // crea elemento
	  arrayDiv.push(document.createElement("div"));


	  arrayDiv[jj].id 				= "div" + fila + jj;

	  // estilos del div
	  arrayDiv[jj].style.width 		= "30px";
	  arrayDiv[jj].style.height 	= "30px";
//	  arrayDiv[ii].style.marginTop 	= "px";


	  arrayDiv[jj].style.display 	= "inline-block";
	  arrayDiv[jj].style.textAlign	= "center";
	  arrayDiv[jj].style.lineHeight = arrayDiv[jj].style.height;
	  arrayDiv[jj].style.verticalAlign = "bottom";
	  if (fila==0){
	  	arrayDiv[jj].style.marginTop = "10px";
 	  }

	  if (jj==0){
		  arrayDiv[jj].style.marginLeft = "16px";
	  }
	  if ((jj%3)==0){
		  arrayDiv[jj].style.borderLeft 	= "2px solid orange";
	  }else{
	  arrayDiv[jj].style.borderLeft 	= "1px solid orange";
	  }
	  if ((fila%3)==0){
			arrayDiv[jj].style.borderTop      = "2px solid orange";
	  }else{
			arrayDiv[jj].style.borderTop      = "1px solid orange";
	  }
	  if (fila==8){
		  arrayDiv[jj].style.borderBottom      = "2px solid orange";
	  }

	  if (jj==8){
		  arrayDiv[jj].style.borderRight       = "2px solid orange";
	  }

//	  arrayDiv[jj].innerHTML = "" + jj;
	  // eventos del div
//	  arrayDiv[jj].onmouseover = FdivOver;
      //arrayDiv[jj].onmouseout  = FdivOut;
//	  arrayDiv[jj].onclick     = FdivClick;
// Esto se paso a la rutina que muestra el sudoku, para habilitar / deshabiltar sensibildad de casillas

	  // agrega div al section
	  section.appendChild(arrayDiv[jj]);
	  }
	  return arrayDiv;

}

function BtnMostrarClick(){
	MostrarSuDoku(matrizSuDoKu,matrizDiv);
}

function MostrarSuDoku(matrizSuDoKu,matrizDiv){
	var xSudo;
	var ySudo;
	var jSudo;
	var kSudo;

	for (var xx = 0;xx < 9;xx++){
		for (var yy = 0;yy < 9;yy++){
			xSudo = parseInt(xx / 3);
			ySudo = parseInt(yy / 3);
			jSudo = xx % 3;
			kSudo = yy % 3;
			if (matrizSuDoKu[xSudo][ySudo][jSudo][kSudo]!=0){
				matrizDiv[xx][yy].innerHTML = "" + matrizSuDoKu[xSudo][ySudo][jSudo][kSudo];
			} else {
				matrizDiv[xx][yy].innerHTML = "";
				matrizDiv[xx][yy].onclick=FdivClick;
				matrizDiv[xx][yy].style.backgroundColor="rgba(0,0,0,0.2";
				matrizDiv[xx][yy].hueco = true;
			}

			//alert("xx:" + xx + " yy:" + yy + " xSudo:" + xSudo + " ySudo:" + ySudo + " jSudo:" + jSudo + " kSudo:" + kSudo +  " valor:" + matrizSuDoKu[xSudo][ySudo][jSudo][kSudo]);

		}
	}

}


function AjustarMatriz(matrizDiv,aBold){
	for (var xx = 0;xx < 9;xx++){
		for (var yy = 0;yy < 9;yy++){
			if (matrizDiv[xx][yy].hueco == true){
				matrizDiv[xx][yy].style.backgroundColor="rgba(0,0,0,0.2";
			} else {
				matrizDiv[xx][yy].style.fontWeight = "normal";
				matrizDiv[xx][yy].style.backgroundColor = "white";
			}
			if (matrizDiv[xx][yy].innerHTML == aBold){
				matrizDiv[xx][yy].style.fontWeight = "bold";
			}
			else{
				matrizDiv[xx][yy].style.fontWeight = "normal";
			}
		}
	}

}

function VerificarSolucion(matrizSuDoKu){
	var arrayRepX = []; // para ver repeticiones en X
	var matrizRepC = [ [], [], []]; // para ver repeticiones en un cuadro 3x3
	var matrizRepY = [ [], [], [],[], [], [],[], [], []]; // para ver repeticiones en columnas

/* la idea general es recorrer por filas. a medida que se recorre se completa:
arrayRepX de la fila en curso
matrizRepC de los tres cuadros 3x3 en curso
matrizRepY de las 9 columnas
De forma tal de recorrer el sudoku una sola vez.
la idea es completar arrayRep de la siguiente forma:
Si el valor del sudoku encontrado es 0, salir (incompleto, no se jugo aun esa posicion)
	se usa como subindice el valor del sudoku.
	si la posicion esta (undefined?), se mueve 1 a la misma.
	Si la posicion NO esta undefined -> error, tengo un numero repetido en la (fila, cuadro, columna)

*/
	var retornar = true;
	for (var xx=0;xx<9;xx++){
		arrayRepX = [];
		for (var yy=0;yy<9;yy++){
			xSudo = parseInt(xx / 3);
			ySudo = parseInt(yy / 3);
			jSudo = xx % 3;
			kSudo = yy % 3;

			if ( (xx==3 && yy ==0) || (xx==6 && yy ==0)) {
				matrizRepC = [ [], [], []];
			}

			// evaluar si la posicion es 0, no se continua.
			if (matrizSuDoKu[xSudo][ySudo][jSudo][kSudo]==0){

//				alert("Hay 0");
				retornar = false;
			}
//          evaluar si hay repeticio en la fila.
//			alert("valor:" + matrizSuDoKu[xSudo][ySudo][jSudo][kSudo] + " x " + xSudo + " y:" + ySudo + " j:"+ jSudo + " k:" + kSudo + "Repe:" + arrayRepX[matrizSuDoKu[xSudo][ySudo][jSudo][kSudo]]);
			if (arrayRepX[matrizSuDoKu[xSudo][ySudo][jSudo][kSudo]] === undefined){
				arrayRepX[matrizSuDoKu[xSudo][ySudo][jSudo][kSudo]] = 1;
			} else {
//				alert("solucion no valida en x " + xSudo + " y:" + ySudo + " j:"+ jSudo + " k:" + kSudo);
//				matrizDiv[xx][yy].style.backgroundColor = "Red";
				retornar = false;
			}


			if (matrizRepC[ySudo][matrizSuDoKu[xSudo][ySudo][jSudo][kSudo]] === undefined ) {
				matrizRepC[ySudo][matrizSuDoKu[xSudo][ySudo][jSudo][kSudo]] = 1
//				alert("ySudo: " + ySudo + " " +  matrizRepC[ySudo] + "XX: " + xx + "YY: " + yy);
			} else {
//				alert("solucion no valida en cuadrado "+ ySudo );
//				matrizDiv[xx][yy].style.backgroundColor = "Red";
				retornar = false;

			}

			if (matrizRepY[yy][matrizSuDoKu[xSudo][ySudo][jSudo][kSudo]] === undefined ) {
				matrizRepY[yy][matrizSuDoKu[xSudo][ySudo][jSudo][kSudo]] = 1
			} else {
//				alert("solucion no valida en col "+ yy );
//				matrizDiv[xx][yy].style.backgroundColor = "Red";
				retornar = false;

			}



//			alert("valor:" + matrizSuDoKu[xSudo][ySudo][jSudo][kSudo] + " x " + xSudo + " y:" + ySudo + " j:"+ jSudo + " k:" + kSudo + "Repe:" + arrayRepX[matrizSuDoKu[xSudo][ySudo][jSudo][kSudo]]);
//			alert("Cuado: " + ySudo + " " +  matrizRepC[ySudo] + "XX: " + xx + "YY: " + yy);

		}
	}
	return retornar;

}



function FdivOver(){


//console.log(event.target.id);
//console.log(event.target.id.substring(3));
//console.log(arrayDiv[parseInt(event.target.id.substring(3))].id);

	arrayDiv[parseInt(event.target.id.substring(3))].style.backgroundColor = "rgba(0,170,0,0.2)";
}

function FdivOut(){
	arrayDiv[parseInt(event.target.id.substring(3))].style.backgroundColor = "rgb(255,255,255)";
}

function FCtrClick(){
	var id = event.target.id.substring(3);
	var value = event.target.innerHTML;
	var ii;
	for (var ii=0 ; ii<10; ii++){
		if (arrayCtr[ii].style.backgroundColor != ""){
			arrayCtr[ii].style.backgroundColor = "";
		}
	}
	event.target.style.backgroundColor = "rgba(255, 191, 114,0.3)";

	AjustarMatriz(matrizDiv, event.target.innerHTML);


}
function FdivClick(){
	var filCol;
	var xx;
	var ii;
	var xSudo;
	var ySudo;
	var jSudo;
	var kSudo;

// traer los ultimos dos caracteres del id del div (como tiene la forma divNN, son los nros a partir de pos 3)
	filCol=event.target.id.substring(3);

// desconcatenar ambos subindices
	xx = filCol[0];
	yy = filCol[1];
//	alert(xx + "  " + yy);

// poner el valor del nro clickeado en el hueco elejida
	for (ii=0 ; ii<10; ii++){
		if (arrayCtr[ii].style.backgroundColor != ""){
			event.target.innerHTML = arrayCtr[ii].innerHTML;
			xSudo = parseInt(xx / 3);
			ySudo = parseInt(yy / 3);
			jSudo = xx % 3;
			kSudo = yy % 3;
			if (arrayCtr[ii].innerHTML == ""){
				matrizSuDoKu[xSudo][ySudo][jSudo][kSudo] = 0;
			}else {
				matrizSuDoKu[xSudo][ySudo][jSudo][kSudo] = arrayCtr[ii].innerHTML;
			}
			break;
		}
	}



	event.target.style.backgroundColor = "rgba(255, 191, 114,0.3)";


	if (VerificarSolucion(matrizSuDoKu)){
					window.location.href = '../src/win.html';;
}

// agregar algunas validaciones, por que pese a ser un input de tipo number deja entrar cosas feas

/*
	if (!isNaN(parseInt(input.value))){
		if (parseInt(input.value) > 9 || parseInt(input.value) < 0){
//		 window.alert("Ingresar unicamente numeros de 0 a 9");
		}else{
// si las validaciones dan lo ponemos en la pantalla y en la matriz interna del sudoku
			// va a matriz interna
			xSudo = parseInt(xx / 3);
			ySudo = parseInt(yy / 3);
			jSudo = xx % 3;
			kSudo = yy % 3;
//			alert("x:" + xx + " y:" + yy + " xSudo " + xSudo + " ySud:" + ySudo + " j:"+ jSudo + " k:" + kSudo + " Valor:" + input.value);
			matrizSuDoKu[xSudo][ySudo][jSudo][kSudo] = parseInt(input.value);
			// va a pantalla
			if (input.value=="0"){
				matrizDiv[xx][yy].innerHTML = "";
//				window.alert(matrizDiv[xx][yy].innerHTML);
			}
			else {
				matrizDiv[xx][yy].innerHTML = "" + parseInt(input.value);
			// si NO se jugo un blanco, verificar
				if (VerificarSolucion(matrizSuDoKu)){
					window.alert("Ganaste!");
				}
			}
		}
	} */

}


function BtnVerifClick(){
	VerificarSolucion(matrizSuDoKu);
}

function CrearArray3Pos(elemUno, elemDos, elemTres){
	var array = [];
	array.push(elemUno);
	array.push(elemDos);
	array.push(elemTres);
	return array;
}


/* Esta funcion NO hace falta, por que es igual que la anterior. En un caso la llamamos con numeros directamente,
y en el otro los elementos son ya arrays de tres posiciones para armar las matrices de 3 * 3
 function CrearTile3x3(){
	var matrizTile = [];
	matrizTile.push(CrearFila(1,2,3));
	matrizTile.push(CrearFila(4,5,6));
	matrizTile.push(CrearFila(7,8,9));
	return matrizTile;
} */

/*function ArrayCircularIzq(array) {
 var arrayLocal = [];
/* la idea es devolver el array entrante, pero con la primer posicion puesta al final.
Ejemplo: si entra [1,2,3] devolver [2,3,1].
PERO, en un NUEVO array, por lo que
array.push(array.shift());
arrayLocal = array
NO SIRVE, eso devuelve por referencia el mismo array que entro, cambiado.

for (var ii=1;ii<array.length;ii++){ // se arranca en 1 adrede omitiendo la primer posicion
	arrayLocal.push(array[ii]);
}
arrayLocal.push(array[0]);

return arrayLocal;

}

function ArrayCircularDer(array) {
 var arrayLocal = [];
/* la idea es devolver el array entrante, pero con la ultima posicion puesta al inicio.
Ejemplo: si entra [1,2,3] devolver [3,1,2].
PERO, en un NUEVO array, por lo que
array.unshift(array.pop());
arrayLocal = array
NO SIRVE, eso devuelve por referencia el mismo array que entro, cambiado.
arrayLocal.push(array[(array.length-1)]);
for (var ii=0;ii<(array.length-1);ii++){ // se arranca en 1 adrede omitiendo la primer posicion
	arrayLocal.push(array[ii]);
}

return arrayLocal;

}*/






function ArrayCircularIzqClone(array) {
 var arrayLocal = [];
 var copy = [];
  copy = Clonar(array);
/* la idea es devolver el array entrante, pero con la primer posicion puesta al final.
Ejemplo: si entra [1,2,3] devolver [2,3,1].
PERO, en un NUEVO array, por lo que
array.push(array.shift());
arrayLocal = array
NO SIRVE, eso devuelve por referencia el mismo array que entro, cambiado.
Ademas, si no se usa el clone, pasa lo mismo cuando array contiene posiciones que son array.
*/

 for (var ii=1;ii<copy.length;ii++){ // se arranca en 1 adrede omitiendo la primer posicion
	 arrayLocal.push(copy[ii]);
 }
 arrayLocal.push(copy[0]);

return arrayLocal;

}


function ArrayCircularDerClone(array) {
 var arrayLocal = [];
/* la idea es devolver el array entrante, pero con la ultima posicion puesta al inicio.
Ejemplo: si entra [1,2,3] devolver [3,1,2].
PERO, en un NUEVO array, por lo que
array.unshift(array.pop());
arrayLocal = array
NO SIRVE, eso devuelve por referencia el mismo array que entro, cambiado.
Ademas, si no se usa el clone, pasa lo mismo cuando array contiene posiciones que son array.
*/
var copy = [];
copy = Clonar(array);
arrayLocal.push(copy[(copy.length-1)]);
for (var ii=0;ii<(copy.length-1);ii++){ // se arranca en 1 adrede omitiendo la primer posicion
	arrayLocal.push(copy[ii]);
}
return arrayLocal;

}


function Clonar( obj ) {
    if ( obj === null || typeof obj  !== 'object' ) {
        return obj;
    }

    var temp = obj.constructor();
    for ( var key in obj ) {
        temp[ key ] = Clonar( obj[ key ] );
    }

    return temp;
}


// genera una matriz aleatoria (De momento Hardcode) que se usa como base para generar un sudoku valido
function GenerarMatrizAleatoria(){
	var arrayTemplate1;
	var arrayTemplate2;
	var arrayTemplate3;
	var matrizAleatoria;
	var arrayRaiz = [];

	var rnd = Math.floor((Math.random() * 9) + 1);
	while (arrayRaiz.length < 9){
		while (arrayRaiz.indexOf(rnd)>=0){
			rnd = Math.floor((Math.random() * 9) + 1);

		}
		arrayRaiz.push(rnd);
	}

	arrayTemplate1 = arrayRaiz.slice(0,3);
	arrayTemplate2 = arrayRaiz.slice(3,6);
	arrayTemplate3 = arrayRaiz.slice(6,9);

//    arrayTemplate1 = CrearArray3Pos(1,2,3);
//    arrayTemplate2 = CrearArray3Pos(4,5,6);
//	arrayTemplate3 = CrearArray3Pos(7,8,9);
	matrizAleatoria = CrearArray3Pos(arrayTemplate1,arrayTemplate2,arrayTemplate3);
	return matrizAleatoria;
}


// a partir de una matriz aleatoria inicial, por desplazamientos circulares de la misma se genera el sudoku
// el mismo tiene una estructura de vector de tres posiciones
// donde a su vez cada vector tiene tres posiciones
// y cada posicion es a su vez una matriz de 3 x 3


function GenerarSuDoKu(matrizTemplate){
	var matr1;
	var matr2;
	var matr3;
	matrizSuDoKu = [];


    matr1 = ArrayCircularDerClone(matrizTemplate);
	matr2 = ArrayCircularDerClone(ArrayCircularIzqClone(matrizTemplate)); // Se hace esto para obtener una copia no una referencia
	matr3 = ArrayCircularIzqClone(matrizTemplate);



	matrizSuDoKu.push(CrearArray3Pos(matr1,matr2,matr3));
//	alert("primeras 3:"+ matrizSuDoKu);

	/* se hace la primer ocurrencia del codigo fuera del loop,
	por que la primer ocurrencia no requiere este tratamiento
	de corrimiento a derecha de las posiciones de la matriz,
	se puede usar como viene -> (por corrimiento hablamos de la linea matrizTemplate[ii] = ArrayCircularDer(matrizTemplate[ii]);)	*/

	for (var jj=0;jj<2;jj++){
		for (var ii=0;ii<3;ii++){

			matrizTemplate[ii] = ArrayCircularDerClone(matrizTemplate[ii]);

		}



    matr1 = ArrayCircularDerClone(matrizTemplate);
	matr2 = ArrayCircularDerClone(ArrayCircularIzqClone(matrizTemplate)); // Se hace esto para obtener una copia no una referencia
	matr3 = ArrayCircularIzqClone(matrizTemplate);


	matrizSuDoKu.push(CrearArray3Pos(matr1,matr2,matr3));
	}

//	alert(matrizSuDoKu);
	return matrizSuDoKu;
}


function RecorrerFila(fila,matriz,col){

	/* si se quita esta declaracion, deja de funcionar!
	El como si al ser recursiva y llamarse a si misma zz conservara el valor en lugar de tomar uno nuevo*/

	for(var zz=0;zz<3;zz++){
		if(typeof(matriz[fila][zz])=="object"){
//			alert("OBJ, Posicion X" + fila + " Y" + zz +  matriz[fila][zz]);
		  RecorrerFila(fila,matriz[fila][zz],zz,0);
		}
		else{
			matrizDiv[0][(col*3+zz)].innerHTML = "" + matriz[fila][zz];
//			alert("Posicion X" + fila + " Y" + zz + " " + matriz[fila][zz] + " col" + col);
		}

	}

}

/*
Analisis para Recorrido por fila orden nat: x y j k

0 0 0 0, 0 0 0 1, 0 0 0 2,
0 1 0 0, 0 1 0 1, 0 1 0 2,
0 2 0 0, 0 2 0 1, 0 2 0 2, (toda la fila 1, 0 en realidad)

orden por fila> parece ser x j y k
0 0 1 0, 0 0 1 1, 0 0 1 2,
0 1 1 0, 0 1 1 1, 0 1 1 2
0 2 1 0, 0 2 1 1, 0 2 1 2



Valores: del 0 al 3

0 0 0 0
0 0 0 1
0 0 0 2


0 0 1 0
0 0 1 1
0 0 1 2


0 0 2 0
0 0 2 1
0 0 2 2

*/
